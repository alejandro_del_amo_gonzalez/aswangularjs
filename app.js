
// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute']);

// Configuración de las rutas
angularRoutingApp.config(function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl	: 'pages/home.html',
			controller 	: 'mainController'
		})
		.when('/submission/:id', {

			templateUrl : 'pages/submission.html',
			controller 	: 'submissionsController'
		})
		.when('/votar/:id', {
			templateUrl : 'pages/home.html',
			controller 	: 'likeController'
		})
		.when('/noticies', {

			templateUrl : 'pages/noticies.html',
			controller 	: 'noticiesController'
		})
		.when('/preguntes', {
			templateUrl : 'pages/preguntes.html',
			controller 	: 'preguntesController'
		})
		.when('/crear', {
			templateUrl : 'pages/crear.html',
			controller 	: 'crearController'
		})
		.when('/editar', {
			templateUrl : 'pages/editar.html',
			controller 	: 'editarController'
		})
		.otherwise({
			redirectTo: '/'
		});
});


angularRoutingApp.controller('mainController', function($http,$scope) {
	//GET API DATA

	$http.get('http://aswlaravel-alexon1234.c9.io/api/submissions').success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
    $scope.datos=data.submissions;
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
    
});



angularRoutingApp.controller('noticiesController', function($scope) {
	$scope.message = 'Esta es la pagina "Noticies"';
});

angularRoutingApp.controller('replyController',  ['$scope','$http', function($scope,$http) {
	$scope.reply = function() {
		var text = $scope.text;
		var id =  $scope.id;
		console.log(text);
		console.log(id);
		var req = $http({
				 method: 'POST',
				 url: 'http://aswlaravel-alexon1234.c9.io/api/submissions/reply/',
				 headers: {'Content-Type': 'application/x-www-form-urlencoded' },
				 data: {
				 	parent : $scope.id,
				 	text : $scope.text
				 }
			});
		       


		    req.success(function (response) {
	            alert("HA FET POST");
	            redirectTo: '/';
	        }).error(function(response) {
	        	alert("NO HA FET POST");
	        })
	}
	
}]);
 
angularRoutingApp.controller('submissionsController', function($sce,$scope,$http,$routeParams) {
   $scope.id = $routeParams.id;
    $http.get('http://aswlaravel-alexon1234.c9.io/api/submissions').success(function(data, status, headers, config) {
    // this callback will be called asynchronously
    // when the response is available
    var hash_array = new Array();
    
    
    //$scope.datos=data.submissions;
    for (var x in data.submissions) {
    	hash_array[data.submissions[x].id] = data.submissions[x];

    }
    $scope.comentaris = "";
    
    
    
    for (var x in hash_array) {
    	console.log(hash_array[x]);
    	if (hash_array[x].parent == $scope.id) {
    		  console.log("entro en el arbol");
              $scope.comentaris += generarArbre(hash_array, hash_array[x].id, 100, 0);
    	}
    }
   
	$scope.comentaris = $sce.trustAsHtml($scope.comentaris);
  	}).error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
  
  function generarArbre(hash_array, id, width, count) {
	var contingut = "";
    count = count +1;
    var value = hash_array[id];
    contingut+= '<div style="border:0px solid;margin-left:'+ (100-width) + '%;width:' + width + '%; min-width:300px;margin-top:4px;">';
	 if(value.descendants == 0) {
            contingut+='<div style="border:0px solid;padding:5px">';
            contingut+='<a style="color:grey;font-size:12px;" href="">' + value.by+'</a><a href="" style="color:grey;font-size:12px;"> at '+value.time+'</a>';
            
            contingut+='<span style="color:grey;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;'+ value.score +'&nbsp;&nbsp;likes&nbsp;&nbsp;</span>';
            
            contingut+='<img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="20" width="20" href="#/votar">'

            contingut+='<p style="color:black;">'+value.texto+'</p>';
            contingut+='<a href="#/submission/'+value.id+'" style="color:black;font-size:12px;text-decoration: underline;">Contestar</a>';
            contingut+='</div>';
	 }
	 else {
	 	    contingut+='<div style="border:0px solid;padding:5px">';
            contingut+='<a style="color:grey;font-size:12px;" href="">'+value.by+'</a><a href="" style="color:grey;font-size:12px;"> at '+value.time+'</a>';
             
            contingut+='<span style="color:grey;font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;  '+ value.score +'&nbsp;&nbsp;likes&nbsp;&nbsp;</span>';
            
            contingut+='<img style="margin-bottom:6px;" src="http://icons.iconarchive.com/icons/iconsmind/outline/128/Like-icon.png" alt="Vote" height="20" width="20" href="#/votar">'
            contingut+='<p style="color:black";>'+value.texto+'</p>';
            contingut+='<a href="#/submission/'+value.id+'" style="color:black;font-size:12px;text-decoration: underline;">Contestar</a>';
            contingut+='</div>';
            var fills = value.kids.split(";");
            for(var i = 0; i < fills.length; ++i) {
            		if (fills[i] !=0) {
                    	contingut+= generarArbre(hash_array, fills[i], 96, count);
            		}
            }
        }
        contingut+="</div>";
        return contingut;
	 }
            
});


angularRoutingApp.controller('preguntesController', function($scope) {
	$scope.message = 'Esta es la pagina de "Preguntes"';
});	

angularRoutingApp.controller('editarController', function($scope) {
	
	//$scope.message = 'Esta es la pagina de "Preguntes"';
	
	

	
});
angularRoutingApp.controller('crearController', ['$scope','$http', function($scope,$http) {
      $scope.submit = function() {
       	var autor = $scope.by;
       	var titol = $scope.titulo;
       	var tipus = $scope.tipo;
       	var texto = $scope.texto;
       	
       
       	 alert(tipus);

       	if(tipus == "url"){//tractem noticies
       		//POST DE CREAR NOTICIA
       		
       		
		       	
			var req = $http({
				 method: 'POST',
				 url: 'http://aswlaravel-alexon1234.c9.io/api/submissions/create/news',
				 headers: {'Content-Type': 'application/x-www-form-urlencoded' },
				 data: {
				 	url : $scope.url,
				 	title : $scope.titulo
				 }
			});
		       


		    req.success(function (response) {
	            alert("HA FET POST url");
	            redirectTo: '/';
	        }).error(function(response) {
	        	alert("NO HA FET POST URL");
	        })
	        
	        
       		
    
       	}
       	
       	else if(tipus=="pregunta"){
       		//POST DE CREAR PREGUNTA
       		
    	 	$http({	
    	 		method: 'POST',
    	 		url: 'http://aswlaravel-alexon1234.c9.io/api/submissions/create/asks',
    	 		headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
    	 		data: {
    	 			text : $scope.texto, 
    	 			title : $scope.titulo 
    	 			
    	 		}
    	 	})
	        .success(function (response) {
	            alert("HA FET POST segon pregunta");
	            redirectTo: '/';
	        })
		    // this callback will be called asynchronously
		    // when the response is available
		   
		  	.error(function(response) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    alert("NO HA FET POST PREGUNTA");
  		  });		
       	}
       	
      };
    }]);
    
    
angularRoutingApp.controller('likeController', function($scope,$http,$routeParams) {

   			$scope.id = $routeParams.id;
   			
   			console.log("Entra");
       		
		       	
			var req = $http({
				 method: 'POST',
				 url: 'http://aswlaravel-alexon1234.c9.io/api/submissions/like',
				 headers: {'Content-Type': 'application/x-www-form-urlencoded' },
				 data: {
				 	id : $scope.id,
				 }
				 
			});
		    req.success(function (response) {
	            alert("HA FET POST url");
				window.location = '/'

	        }).error(function(response) {
	        	alert("NO HA FET POST URL");
	        })
	        
	        
       		
    
       	
       	
      
    });


/*var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
    $http.get("http://www.w3schools.com/angular/customers.php")
    .success(function (response) {$scope.names = response.records;});
});*/